%global         debug_package %{nil}
%global         __strip /bin/true
%global         __requires_exclude libffmpeg.so
%global         __provides_exclude_from ^%{_libdir}/%{name}/.*\\.so
%global         _build_id_links none

Name:           obsidian
Version:        0.15.9
Release:        %autorelease
Summary:        Obsidian is a powerful knowledge base on top of a local folder of plain text Markdown files.

License:        Proprietary
URL:            https://obsidian.md
Source0:        https://github.com/obsidianmd/obsidian-releases/releases/download/v%{version}/%{name}-%{version}.tar.gz
Source1:        %{name}.png
Source2:        %{name}.desktop

BuildRequires:  desktop-file-utils

%description
Obsidian is a powerful knowledge base on top of a local folder of plain text Markdown files.

%prep
%autosetup

%build

%install
mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_libdir}/%{name}
mkdir -p %{buildroot}/%{_datadir}/applications
cp -r * %{buildroot}/%{_libdir}/%{name}/
install -D %{SOURCE1} %{buildroot}%{_datadir}/icons/hicolor/512x512/apps/%{name}.png
ln -s %{_libdir}/%{name}/%{name} %{buildroot}/%{_bindir}/
desktop-file-install                            \
--set-icon=%{name}                              \
--set-key=Exec --set-value=%{_bindir}/%{name}   \
--dir=%{buildroot}/%{_datadir}/applications     \
%{SOURCE2}

%files
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/512x512/apps/%{name}.png
%{_libdir}/%{name}/

%changelog
* Tue Dec 28 2021 zhou
- Update 0.13.14
